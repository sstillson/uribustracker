package edu.uri.uribustracker;

import org.junit.Test;

import static org.junit.Assert.*;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ray on 4/25/16.
 */
public class RouteTest {

    @Test
    public void testClosestPoint() throws Exception {
        double coords[] = {0, 0, 1, 1};
        Route r = new Route(coords, 1, 16.0f);
        LatLng l = new LatLng(2, 2);
        Route.InterpolationResult result = r.closestPoint(l);
        Util.printLatLong(result.point);
        assertEquals(result.point.latitude, 1);
        assertEquals(result.point.longitude, 1);
    }
}
