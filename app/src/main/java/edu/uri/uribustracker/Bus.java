package edu.uri.uribustracker;

/**
 * Created by ray on 4/17/16.
 *
 * The Bus class holds all of the data for the buses along the routes.
 * This class generates the location, vehicle number, position, and
 * the route of each bus.
 *
 * @author emilyhendricks
 */
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.model.LatLng;
import java.util.Date;

public class Bus {
    public LatLng realPos;
    public Date lastUpdate;
    public Route.DynamicInterpolationResult lastInterpolatedPos;
    public Route.DynamicInterpolationResult interpolatedPos;
    public boolean pendingRedraw = false;
    public Route mostLikelyRoute() {
        if (redRouteWeight < blueRouteWeight) {
            return Route.getHillClimberRoute();
        } else {
            return Route.getBlueRoute();
        }
    }
    private double redRouteWeight = 0;
    private double blueRouteWeight = 0;
    public void setInterpolatedPos(Route.DynamicInterpolationResult pos) {
        if (pos.route == Route.getHillClimberRoute()) {
            redRouteWeight += Math.pow(pos.result.distance, 2);
            blueRouteWeight += Math.pow(Route.getBlueRoute().closestPoint(pos.result.point).distance, 4);
        } else {
            blueRouteWeight += Math.pow(pos.result.distance, 4);
            redRouteWeight += Math.pow(Route.getBlueRoute().closestPoint(pos.result.point).distance, 2);
        }
        interpolatedPos = pos;
    }
    public String time;
    public String route;
    public String vehicle;
    public Marker marker;
    public MarkerOptions markerOpts;
}
