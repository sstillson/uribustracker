package edu.uri.uribustracker;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;

/**
 * Created by ray on 4/17/16.
 *
 * The BusStop class draws the icons that indicate where each of the
 * bus stops are along the two routes.
 * It places the blue icons on the Blue line bus routes and the red
 * icons on the Hill Climber routes.
 *
 * @author emilyhendricks
 */
public class BusStop {
    public BusStop(String name, double latitude, double longitude, String color) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        if (color.equals("red")) this.route = Route.getHillClimberRoute();
        else if (color.equals("blue")) this.route = Route.getBlueRoute();
    }
    public BitmapDescriptor getIcon() {
        if (this.route == Route.getBlueRoute()) return BitmapDescriptorFactory.fromResource(R.mipmap.bluepin);
        else return BitmapDescriptorFactory.fromResource(R.mipmap.redpin);
    }
    public String name;
    public double latitude;
    public double longitude;
    public Route route;
}
