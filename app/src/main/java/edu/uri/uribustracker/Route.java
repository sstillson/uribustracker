package edu.uri.uribustracker;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.ArrayList;
import java.util.Iterator;
import android.graphics.Color;
import android.view.animation.AccelerateDecelerateInterpolator;
/**
 * Created by ray on 4/24/16.
 *
 * This class specifies all of the coordinates along the two different
 * routes, the Hill Climber and the Blue Line. These coordinates are
 * used to draw the routes on the maps in their specified locations.
 * This also helps determine where exactly along the routes the buses
 * are located.
 *
 * @author emilyhendricks
 *
 */
public class Route {
    private static double HILL_CLIMBER_COORDINATES[] = {
            41.491742, -71.536148,
            41.490814, -71.529845,
            41.490452, -71.525961,
            41.490054, -71.524132,
            41.488355, -71.52499,
            41.488439, -71.525376,
            41.488487, -71.526766,
            41.488339, -71.527098,
            41.488371, -71.527538,
            41.488214, -71.52808,
            41.488162, -71.529786,
            41.489082, -71.536867,
            41.489353, -71.539356,
            41.491784, -71.538862,
            41.491877, -71.538492,
            41.491912, -71.537311,
            41.491739, -71.536168
    };
    private static double BLUE_ROUTE_COORDINATES[] = {
            41.488355, -71.52499,
            41.488439, -71.525376,
            41.488487, -71.526766,
            41.488339, -71.527098,
            41.488371, -71.527538,
            41.488214, -71.52808,
            41.488162, -71.529786,
            41.489082, -71.536867,
            41.489356, -71.53934,
            41.489353, -71.539356,
            41.491784, -71.538862,
            41.491877, -71.538492,
            41.491912, -71.537311,
            41.491739, -71.536168,
            41.490814, -71.529845,
            41.490452, -71.525961,
            41.490054, -71.524132,
            41.488355, -71.52499,
            41.486116, -71.525886,
            41.485634, -71.525955,
            41.485739, -71.528836,
            41.485686, -71.528997,
            41.48442, -71.529072,
            41.484272, -71.529303,
            41.484095, -71.529147,
            41.483548, -71.529276,
            41.484002, -71.530542,
            41.484043, -71.531706,
            41.484175, -71.53184,
            41.485839, -71.531744,
            41.48585, -71.53263,
            41.48453, -71.53264,
            41.48453, -71.53285,
            41.48401, -71.53314,
            41.4838, -71.53319,
            41.48346, -71.53327,
            41.4839, -71.53521,
            41.48431, -71.53503,
            41.48469, -71.53502,
            41.48474, -71.53608,
            41.48437, -71.53617,
            41.48375, -71.53673,
            41.48309, -71.53745,
            41.4827, -71.53763,
            41.48257, -71.53668,
            41.48306, -71.53608,
            41.48347, -71.53553,
            41.4839, -71.53521,
            41.48346, -71.53327,
            41.4831, -71.53184,
            41.48209, -71.53218,
            41.48228, -71.53267,
            41.48231, -71.53313,
            41.48253, -71.53432,
            41.48303, -71.53428,
            41.48364, -71.53406,
            41.48346, -71.53327,
            41.4839, -71.53521,
            41.48347, -71.53553,
            41.48306, -71.53608,
            41.48257, -71.53668,
            41.4827, -71.53763,
            41.48309, -71.53745,
            41.48375, -71.53673,
            41.48437, -71.53617,
            41.48474, -71.53608,
            41.48469, -71.53502,
            41.4843, -71.53503,
            41.4839, -71.53521,
            41.48346, -71.53327,
            41.4838, -71.53319,
            41.48401, -71.53314,
            41.48453, -71.53285,
            41.48453, -71.53264,
            41.48585, -71.53263,
            41.485839, -71.531744,
            41.484175, -71.53184,
            41.484043, -71.531706,
            41.484002, -71.530542,
            41.483548, -71.529276,
            41.484095, -71.529147,
            41.484272, -71.529303,
            41.48442, -71.529072,
            41.485686, -71.528997,
            41.485739, -71.528836,
            41.485634, -71.525955,
            41.486116, -71.525886
    };
    private static double accelerateDecelerateInterpolator(double t) {
        return Math.pow(Math.cos((t + 1)*Math.PI/2), 2);
    }
    public ArrayList<LatLng> coords;
    public float width;
    public int color;
    private PolylineOptions polylineOptions;

    public Route(double[] coords, int color, float width) {
        this.coords = new ArrayList<LatLng>();
        for (int i = 0; i < coords.length; i += 2) {
            this.coords.add(new LatLng(coords[i], coords[i + 1]));
        }
        this.color = color;
        this.width = width;
        this.polylineOptions = new PolylineOptions().color(color).width(width);
        for (Iterator<LatLng> it = this.coords.iterator(); it.hasNext();) {
            this.polylineOptions.add(it.next());
        }
    }
    private double distance(LatLng a, LatLng b, LatLng c, double latitude) {
        return Math.sqrt(Math.pow(latitude - c.latitude, 2) + Math.pow((b.longitude - a.longitude) * (latitude - a.latitude) / (b.latitude - a.latitude) + a.longitude - c.longitude, 2));
    }
    private double square(double d) { return Math.pow(d, 2); }
    private double minDistance(LatLng x, LatLng y, LatLng z) {
        double a = y.latitude;
        double h = z.latitude;
        double j = x.longitude;
        double i = x.latitude;
        double b = y.longitude;
        double f = z.longitude;

        return (square(a)*h + a*b*f - a*b*j - a*f*j - 2*a*h*i + a*square(j) + square(b)*i - b*f*i - b*i*j + f*i*j + h*square(i))/(square(a) - 2*a*i + square(b) - 2*b*j + square(i) + square(j));
    }
    /*
    private double minDistance(LatLng a, LatLng b, LatLng c) {
        return (Math.pow(b.latitude, 2)*c.latitude + b.latitude*b.longitude*c.longitude - b.latitude*c.longitude*a.longitude - 2*b.latitude*c.latitude*a.latitude + b.latitude*Math.pow(a.longitude, 2) + Math.pow(b.longitude, 2)*a.latitude - b.longitude*c.longitude*a.latitude - b.longitude*a.longitude*a.latitude + c.longitude*a.longitude*a.latitude + c.latitude*Math.pow(a.latitude, 2))/(Math.pow(b.latitude, 2) - 2*b.latitude*a.latitude + Math.pow(b.longitude, 2) - 2*b.longitude*a.longitude + Math.pow(a.longitude, 2) + Math.pow(a.latitude, 2));
    }
    */
    private double getInterpolatedLongitude(LatLng a, LatLng b, double latitude) {
        return (b.longitude - a.longitude)*(latitude - a.latitude)/(b.latitude - a.latitude) + a.longitude;
    }
    public static class InterpolationResult {
        public LatLng point;
        public double distance;
        public int pathSegment;
        public InterpolationResult(LatLng pt, double d, int p) { point = pt; distance = d; pathSegment = p; }
    }
    public InterpolationResult closestPoint(LatLng pt) {
        double max = Double.MAX_VALUE;
        InterpolationResult retval = new InterpolationResult(new LatLng(0, 0), 0, 0);
        for (int i = 0; i < coords.size() - 1; i++) {
            double x = minDistance(coords.get(i), coords.get(i + 1), pt);
            double dis = distance(coords.get(i), coords.get(i + 1), pt, x);
  /*          int furtherEast;
            int furtherWest;
            if (coords.get(i + 1).latitude > coords.get(i).latitude) {
                furtherEast = i + 1;
                furtherWest = i;
            } else {
                furtherEast = i;
                furtherWest = i + 1;
            } */
            int furtherWest = i;
            int furtherEast = i + 1;
            if (x < coords.get(furtherWest).latitude) {
                dis += Math.sqrt(Math.pow(coords.get(furtherWest).latitude - x, 2) + Math.pow(coords.get(furtherWest).longitude - getInterpolatedLongitude(coords.get(i), coords.get(i + 1), x), 2));
            } else if (x > coords.get(furtherEast).latitude) {
                dis += Math.sqrt(Math.pow(x - coords.get(furtherEast).latitude, 2) + Math.pow(getInterpolatedLongitude(coords.get(i), coords.get(i + 1), x) - coords.get(furtherEast).longitude, 2));
            }
            if (dis < max) {
                max = dis;
                LatLng result;
                if (x < coords.get(furtherWest).latitude) {
                    result = coords.get(furtherWest);
                } else if (x > coords.get(furtherEast).latitude) {
                    result = coords.get(furtherEast);
                } else result = new LatLng(x, getInterpolatedLongitude(coords.get(i), coords.get(i + 1), x));
                retval = new InterpolationResult(result, dis, i);
            }
        }
        return retval;
    }
    public static class PathFollowingResult {
        public double distance;
        public boolean isIncreasing;
        public PathFollowingResult(double d, boolean b) { this.distance = d; this.isIncreasing = b; }
    }
    public double log(double d) { System.out.println(d); return d; }
    public PathFollowingResult inIncreasingDirection(InterpolationResult start, InterpolationResult end) {
        double lat = start.point.latitude;
        double longitude = start.point.longitude;
        double totalPos = 0;
        for (int i = 0; i < coords.size(); i++) {
            if (Math.abs(end.point.longitude - ((coords.get((start.pathSegment + i + 1) % coords.size()).longitude - coords.get((start.pathSegment + i) % coords.size()).longitude)*(end.point.latitude - coords.get((start.pathSegment + i) % coords.size()).latitude)/(coords.get((start.pathSegment + i + 1) % coords.size()).latitude - coords.get((start.pathSegment + i) % coords.size()).latitude) + coords.get((start.pathSegment + i) % coords.size()).longitude)) < 0.001) {
                totalPos += Math.sqrt(Math.pow(end.point.latitude - lat, 2) + Math.pow(end.point.longitude - longitude, 2));
                break;
            } else {
                totalPos += Math.sqrt(Math.pow(coords.get((start.pathSegment + i + 1) % coords.size()).longitude - coords.get((start.pathSegment + i) % coords.size()).longitude, 2) + Math.pow(coords.get((start.pathSegment + i + 1) % coords.size()).latitude - coords.get((start.pathSegment + i) % coords.size()).latitude, 2));
            }
            lat = coords.get((start.pathSegment + i + 1) % coords.size()).latitude;
            longitude = coords.get((start.pathSegment + i + 1) % coords.size()).longitude;
        }
        lat = start.point.latitude;
        longitude = start.point.longitude;
        double totalNeg = 0;
        for (int i = 0; i < coords.size(); i++) {
            if (Math.abs(end.point.longitude - ((coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).longitude - coords.get((start.pathSegment - i + coords.size()) % coords.size()).longitude)*(end.point.latitude - coords.get((start.pathSegment - i + coords.size()) % coords.size()).latitude)/(coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).latitude - coords.get((start.pathSegment - i + coords.size()) % coords.size()).latitude) + coords.get((start.pathSegment - i + coords.size()) % coords.size()).longitude)) < 0.001) {
                totalNeg += Math.sqrt(Math.pow(end.point.latitude - lat, 2) + Math.pow(end.point.longitude - longitude, 2));
                break;
            } else {
                totalNeg += Math.sqrt(Math.pow(coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).longitude - coords.get((start.pathSegment - i + coords.size()) % coords.size()).longitude, 2) + Math.pow(coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).latitude - coords.get((start.pathSegment - i + coords.size()) % coords.size()).latitude, 2));
            }
            lat = coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).latitude;
            longitude = coords.get((start.pathSegment - (i + 1) + coords.size()) % coords.size()).longitude;
        }
        if (totalPos < totalNeg) return new PathFollowingResult(totalPos, true);
        else return new PathFollowingResult(totalNeg, false);
    }
    public static class DynamicInterpolationResult {
        public Route route;
        public InterpolationResult result;
        public DynamicInterpolationResult(Route r, InterpolationResult res) {
            route = r;
            result = res;
        }
    }
    public static LatLng dynamicPointAlongPath(DynamicInterpolationResult start, DynamicInterpolationResult end, int step, int max) {
        if (start.route != end.route) {
            InterpolationResult newStart = end.route.closestPoint(start.result.point);
            return end.route.pointAlongPath(newStart, end.result, step, max);
        } else {
            return end.route.pointAlongPath(start.result, end.result, step, max);
        }
    }
    public LatLng pointAlongPath(InterpolationResult start, InterpolationResult end, int step, int max) {
        PathFollowingResult follow = inIncreasingDirection(start, end);
        int sign = 1;
        if (!follow.isIncreasing) sign = -1;
        double distance = follow.distance*accelerateDecelerateInterpolator((double) step/max);
        double total = 0;
        double lat = start.point.latitude;
        double longitude = start.point.longitude;
        for (int i = 0; i < coords.size(); i++) {
            double dis = Math.sqrt(Math.pow(coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).longitude - coords.get((start.pathSegment + sign*i + coords.size()) % coords.size()).longitude, 2) + Math.pow(coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).latitude - coords.get((start.pathSegment + sign*i + coords.size()) % coords.size()).latitude, 2));
            if (distance < dis) {
                double ratio = distance/dis;
                lat += (coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).latitude - lat)*ratio;
                longitude += (coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).longitude - longitude)*ratio;
                break;
            }
            lat = coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).latitude;
            longitude = coords.get((start.pathSegment + sign*(i + 1) + coords.size()) % coords.size()).longitude;
            distance -= dis;
        }
        LatLng retval = new LatLng(lat, longitude);
        return retval;
    }
    public static DynamicInterpolationResult closestPointAllRoutes(LatLng pt) {
        double max = Double.MAX_VALUE;
        InterpolationResult blue = Route.getBlueRoute().closestPoint(pt);
        InterpolationResult red = Route.getHillClimberRoute().closestPoint(pt);
        if (blue.distance < red.distance) return new DynamicInterpolationResult(Route.getBlueRoute(), blue);
        else return new DynamicInterpolationResult(Route.getHillClimberRoute(), red);
    }
    private static Route blueInstance;
    private static Route redInstance;
    public static Route getHillClimberRoute() {
        if (redInstance == null) {
            redInstance = new Route(HILL_CLIMBER_COORDINATES, Color.RED, 16.0f);
        }
        return redInstance;
    }
    public PolylineOptions getPolylineOptions() {
        return polylineOptions;
    }
    public static Route getBlueRoute() {
        if (blueInstance == null) {
            blueInstance = new Route(BLUE_ROUTE_COORDINATES, Color.BLUE, 8.0f);
        }
        return blueInstance;
    }
}
