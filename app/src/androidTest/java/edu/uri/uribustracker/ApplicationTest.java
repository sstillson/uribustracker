package edu.uri.uribustracker;

import android.app.Application;
import android.test.ApplicationTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import android.graphics.Color;
import com.google.android.gms.maps.model.LatLng;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
}